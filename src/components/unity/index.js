import { useEffect } from "react";
import { Unity, useUnityContext } from "react-unity-webgl";
export default function GameStart({address}) {
  const unityContext = useUnityContext({
    loaderUrl: "/build/PepeBricks.loader.js",
    dataUrl: "/build/PepeBricks.data",
    frameworkUrl: "/build/PepeBricks.framework.js",
    codeUrl: "/build/PepeBricks.wasm",
  });

  useEffect(() => {
    unityContext.sendMessage(
      "JavascriptHook",
      "GetJson",
      JSON.stringify({
        wallet: address,
      })
    );
  }, [unityContext , address]);
  return (
    <div className="w-full h-full flex items-center justify-center py-4">
      {/* بارگذاری بازی Unity */}
      <Unity unityProvider={unityContext.unityProvider} className="w-full h-full max-w-md min-h-screen"/>
    </div>
  );
}
