import GameStart from "@/components/unity";
import { Web3Button } from "@web3modal/react";
import { useAccount, useContract } from "wagmi";
const HomePage = () => {
  const { address , connector , isConnected , isConnecting , isDisconnected , isReconnecting , status } = useAccount();
  console.log("address : ", address)
  console.log("connector : " , connector)
  console.log("connect : " , isConnected)
  console.log("connecting : " , isConnecting)
  console.log("dis connect : " , isDisconnected)
  console.log("re connect : " , isReconnecting)
  console.log("status : " , status)
  // console.log("address : " , address)
  
  return (
    <div>
      {/* <GameStart /> */}
      <Web3Button />
    </div>
  );
};

export default HomePage;
