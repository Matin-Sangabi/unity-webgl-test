import GameStart from "@/components/unity";
import { useAccount } from "wagmi";

const Page2 = () => {
  const { address } = useAccount();
  console.log(address);
    return <>
        <h1>address : {address} </h1>
        {address !== undefined ? <GameStart address={address} /> : null}
    </>;
};

export default Page2;
